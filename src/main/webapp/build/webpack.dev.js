'use strict';
process.env.NODE_ENV = 'development';

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const base = require('./webpack.base');
const _ = require('./utils');
const FriendlyErrors = require('friendly-errors-webpack-plugin');

base.devtool= '#cheap-module-eval-source-map';
base.plugins.push(
  new HtmlWebpackPlugin({
    title: 'DEVELOPMENT!!!',
    template: path.resolve(__dirname, 'index.html'),
    filename: path.join(__dirname, '../dist/static/index.html')
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development')
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new FriendlyErrors()
);

// push loader for css files
_.cssProcessors.forEach(processor => {
  let loaders;
  if (processor.loader === '') {
    loaders = ['postcss-loader']
  } else {
    loaders = ['postcss-loader', processor.loader]
  }
  base.module.loaders.push(
    {
      test: processor.test,
      loaders: ['style-loader', _.cssLoader].concat(loaders)
    }
  )
});

module.exports = base;
