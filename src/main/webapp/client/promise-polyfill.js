import Promise from 'promise-polyfill'
window.Promise = window.Promise || Promise;

window.addEventListener("unhandledrejection", function(event) {
  event.preventDefault();
});
