import { EMPLOYEE_LIST, EMPLOYEE_ACTIVATE, EMPLOYEE_DEACTIVATE } from '../../mutationTypes'
import _ from 'lodash'

const mutations = {
  [EMPLOYEE_LIST](state, action){
    state.items = action.items
  },
  [EMPLOYEE_ACTIVATE](state, id){
    _.find(state.items, {id: id}).active = true;
  },
  [EMPLOYEE_DEACTIVATE](state, id){
    _.find(state.items, {id: id}).active = false;
  }
};

export default mutations;
