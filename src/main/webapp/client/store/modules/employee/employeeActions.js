import * as types from "../../mutationTypes";
import employeeService from "../../../services/employeeService";
import _ from 'lodash';

const actions = {
  async getAll ({ state, dispatch, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      dispatch('app/loadingDataEnable', null, {root: true});
      const employees = await employeeService.findAll();
      await commit(types.EMPLOYEE_LIST, {
        items: employees
      });
      dispatch('app/loadingDataDisable', null, {root: true});
    }
  },
  async create ({ dispatch, commit}, employee) {
    try {
      let response = await employeeService.create(employee);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch, commit}, employee) {
    try {
      let response = await employeeService.update(employee);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async activate ({ state, commit}, id) {
    try {
      await commit(types.EMPLOYEE_ACTIVATE, id);
      let employee = _.find(state.items, { id: id});
      let response = await employeeService.update(employee);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async deactivate ({ state, commit}, id) {
    try {
      await commit(types.EMPLOYEE_DEACTIVATE, id);
      let employee = _.find(state.items, { id: id});
      let response = await employeeService.update(employee);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await employeeService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  }
};

export default actions
