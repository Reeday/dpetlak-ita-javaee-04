'use strict';

import notificationService from 'services/notificationService';

export default (store) => {
  return (request, next) => {
    // do logout action if 401 is returned
    next(function(response) {
      if (response.status !== 200) {
        //authentication error
        if (response.data && response.data.error === 'invalid_grant') {
          notificationService.error('error.auth.login');
        //validation failure
        } else if (response.status === 400) {
          if (response.data.errors && response.data.errors.length && response.data.errors[0].code) {
            notificationService.error('error.' + response.data.errors[0].code, 'error.data.invalid');
          } else {
            notificationService.error('error.' + response.data.message, [{args: response.data.args}], 'error.data.invalid');
          }
        }
        //unauthorized error
        else if (response.status === 401) {
          notificationService.error('error.auth.invalid');
        }
        //invalid request 404
        else if (response.status === 404) {
          notificationService.error('error.data.notFound');
          store.dispatch('app/loadingDataDisable');
        }
        //general error
        else if (response.data && response.data.message) {
          // console.error(VueNotifications.error);
          notificationService.error(response.data.message);
          if (response.data.stackTrace) {
            console.error('error: ', response.data.stackTrace);
          }
        } else {
          notificationService.error('error.application.common');
        }

        console.log(response)
      }
    });
  }
}
