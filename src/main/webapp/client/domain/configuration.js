import _ from 'lodash';

class Configuration {
  constructor(data) {
    _.merge(this, data);
  }
}

export default Configuration;
