import _ from 'lodash';
import moment from 'moment';

class User {
  constructor(data) {
    _.merge(this, data);
    this.created = data && data.created ? moment(data.created) : null;
  }

  get fullName() {
    return this.firstname + ' ' + this.surname;
  }
}

export default User;
