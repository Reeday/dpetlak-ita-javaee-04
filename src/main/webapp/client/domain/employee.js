import _ from 'lodash';
import moment from 'moment';

class Employee {
  constructor(data) {
    _.merge(this, data);
    this.created = data.created ? moment(data.created) : null;
  }

  get fullName() {
    return this.firstName + ' ' + this.surname;
  }
}

export default Employee;
