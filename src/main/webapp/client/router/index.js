import Vue from 'vue'
import Router from 'vue-router'
import loginView from 'views/login'
import usersView from 'views/users'
import companiesView from 'views/companies'
import employeesView from 'views/employees'
import tmProjectView from 'views/tmProject'
import tmProjectsView from 'views/tmProjects'
import overviewView from 'views/overview'
import profileView from 'views/profile'
import profileEditView from 'views/profileEdit'
import passwordChangeView from 'views/passwordChange'

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/overview'
    },
    {
      name: 'login',
      path: '/login',
      component: loginView,
      meta: {
        bodyClass: 'login-page'
      }
    },
    {
      path: '/overview',
      component: overviewView,
      meta: {
        requiresLoggedIn: true
      }
    },
    {
      path: '/profile',
      component: profileView,
      meta: {
        requiresLoggedIn: true
      }
    },
    {
      path: '/profile-edit',
      component: profileEditView,
      meta: {
        requiresLoggedIn: true
      }
    },
    {
      path: '/password-change',
      component: passwordChangeView,
      meta: {
        requiresLoggedIn: true
      }
    },
    {
      path: '/companies',
      component: companiesView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'ADMIN'
      }
    },
    {
      path: '/employees',
      component: employeesView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'ADMIN'
      }
    },
    {
      name: 'tmProjectDetail',
      path: '/tm-project/:id',
      component: tmProjectView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'DEALER'
      }
    },
    {
      name: 'tmProjects',
      path: '/tm-projects',
      component: tmProjectsView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'DEALER'
      }
    },
    {
      path: '/users',
      component: usersView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'ADMIN'
      }
    },
    {
      path: '/*',
      redirect: '/'
    }
  ]
});

export default router;

