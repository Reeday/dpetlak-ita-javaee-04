'use strict';

import moment from 'moment';

export default (value) => {
  return !!value ? moment(value).format("MM/YYYY") : null;
}
