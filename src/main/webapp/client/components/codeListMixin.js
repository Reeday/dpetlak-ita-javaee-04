import {alert} from 'vue-strap';
import {mapState} from 'vuex';
import notificationService from 'services/notificationService';
import codeListModal from 'components/codeListCreateModalGeneric';

export default {
  components: { alert, codeListModal },
  props: {
    title: {
      type: String,
      required: true
    },
    moduleName: {
      type: String,
      required: true
    }
  },
  data: () => ({
    showCreateModal: false,
    selectedItem: null,
    query: null
  }),
  computed: mapState({
    items(state) {
      const items = state[this.moduleName].items;
      if (this.query) {
        let regexp = new RegExp(this.query.toLowerCase(), "g");
        return items.filter(item => !!item.label.toLowerCase().match(regexp));
      } else {
        return items;
      }
    }
  }),
  methods: {
    createShow () {
      this.selectedItem = {};
      this.showCreateModal = true;
    },
    createHide () {
      this.showCreateModal = false;
    },
    deleteItem (item) {
      this.confirm('codeList.delete.confirmation', [item.label]).then(() => {
        this.$store.dispatch(this.moduleName + '/delete', item.id).then((result) => {
          if (result) {
            notificationService.success('Položka bola zmazaná.');
          }
          this.unprotect();
        }).catch(this.unprotect);
      });
    },
    editItem (item) {
      this.selectedItem = _.cloneDeep(item);
      this.showCreateModal = true;
    },
    deactivateItem (item) {
      this.$store.dispatch(this.moduleName + '/deactivate', item.id).then((result) => {
        if (result) {
          notificationService.success('Položka bola deaktivovaná.');
        }
      });
    },
    activateItem (item) {
      this.$store.dispatch(this.moduleName + '/activate', item.id).then((result) => {
        if (result) {
          notificationService.success('Položka bola aktivovaná.');
        }
      });
    }
  },
  created () {
    this.$store.dispatch(this.moduleName + '/getAll')
  }
}
