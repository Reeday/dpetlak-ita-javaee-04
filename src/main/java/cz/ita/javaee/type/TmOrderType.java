package cz.ita.javaee.type;

public enum TmOrderType {
    INCOMING,
    DELIVERY
}