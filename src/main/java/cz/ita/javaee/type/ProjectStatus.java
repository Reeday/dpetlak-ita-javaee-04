package cz.ita.javaee.type;

public enum ProjectStatus {
    OPEN,
    CLOSED
}
