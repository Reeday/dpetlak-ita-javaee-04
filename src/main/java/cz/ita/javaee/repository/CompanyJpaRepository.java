package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

@Profile("jpa")
@Repository
public class CompanyJpaRepository implements CompanyRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CompanyEntity findOne(long id) {
        return entityManager.find(CompanyEntity.class, id);
    }

    @Override
    public List<CompanyEntity> find(int offset, int limit) {
        Query findAll = entityManager.createQuery("SELECT c FROM CompanyEntity AS c", CompanyEntity.class);
        findAll.setFirstResult(offset);
        findAll.setMaxResults(limit);
        return findAll.getResultList();
    }

    @Override
    public void delete(long id) {
        CompanyEntity ref = entityManager.getReference(CompanyEntity.class, id);
        entityManager.remove(ref);
    }

    @Override
    public CompanyEntity update(CompanyEntity entity) {
        entityManager.merge(entity);
        return entity;
    }

    @Override
    public CompanyEntity create(CompanyEntity entity) {
        entity.setCreated(LocalDateTime.now());
        entityManager.persist(entity);
        return entity;
    }
}
