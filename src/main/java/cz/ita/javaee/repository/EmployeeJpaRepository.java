package cz.ita.javaee.repository;

import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

@Profile("jpa")
@Repository
public class EmployeeJpaRepository implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EmployeeEntity findOne(long id) {
        return entityManager.find(EmployeeEntity.class, id);
    }

    @Override
    public List<EmployeeEntity> find(int offset, int limit) {
        Query findAll = entityManager.createQuery("SELECT e FROM EmployeeEntity AS e", EmployeeEntity.class);
        findAll.setFirstResult(offset);
        findAll.setMaxResults(limit);
        return findAll.getResultList();
    }

    @Override
    public void delete(long id) {
        EmployeeEntity ref = entityManager.getReference(EmployeeEntity.class, id);
        entityManager.remove(ref);
    }

    @Override
    public EmployeeEntity update(EmployeeEntity entity) {
        entityManager.merge(entity);
        return entity;
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        entity.setCreated(LocalDateTime.now());
        entityManager.persist(entity);
        return entity;
    }
}
