package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Profile("inMemory")
@Repository
public class CompanyInMemoryRepository implements CompanyRepository {

    private final List<CompanyEntity> DATA = new ArrayList<>();

    private AtomicLong idGenerator = new AtomicLong(3);

    @PostConstruct
    public void init() {
        CompanyEntity company1 = new CompanyEntity();
        company1.setId(1L);
        company1.setName("Best soft s.r.o.");
        company1.setCompanyId("11223344");
        company1.setVatId("CY11223344");
        company1.setActive(true);
        company1.setOwnershipped(true);
        company1.setNote("Hlavní firma.");
        company1.setCreated(LocalDateTime.now());
        AddressEntity addressCompany1 = new AddressEntity();
        addressCompany1.setStreetName("Lidická");
        addressCompany1.setZipCode("60200");
        addressCompany1.setCity("Brno");
        addressCompany1.setHouseNumber("12");
        addressCompany1.setCountry("CZE");
        company1.setAddress(addressCompany1);

        CompanyEntity company2 = new CompanyEntity();
        company2.setId(2L);
        company2.setName("Wekolo");
        company2.setCompanyId("11112222");
        company2.setVatId("SR11112222");
        company2.setActive(true);
        company2.setOwnershipped(false);
        company2.setCreated(LocalDateTime.now());
        AddressEntity addressCompany2 = new AddressEntity();
        addressCompany2.setStreetName("Farska");
        addressCompany2.setZipCode("01898");
        addressCompany2.setCity("Stropkov");
        addressCompany2.setHouseNumber("1/a");
        addressCompany2.setCountry("SVK");
        company2.setAddress(addressCompany2);

        DATA.add(company1);
        DATA.add(company2);
    }

    @Override
    public CompanyEntity findOne(long id) {
        return DATA.stream().filter(companyEntity -> companyEntity.getId().equals(id))
                .findFirst()
                .orElse(null);

    }

    @Override
    public List<CompanyEntity> find(int offset, int limit) {
        return DATA.subList(offset, Math.min(offset + limit, DATA.size()));
    }

    @Override
    public void delete(long id) {
        DATA.removeIf(companyDto -> companyDto.getId().equals(id));
    }

    @Override
    public CompanyEntity update(CompanyEntity entity) {
        return DATA.stream()
                .filter(companyEntity -> companyEntity.getId().equals(entity.getId()))
                .map(companyEntity -> {
                    companyEntity.setName(entity.getName());
                    companyEntity.setCompanyId(entity.getCompanyId());
                    companyEntity.setVatId(entity.getVatId());
                    companyEntity.setActive(entity.isActive());
                    companyEntity.setOwnershipped(entity.isOwnershipped());
                    companyEntity.setCreated(entity.getCreated());
                    AddressEntity addressCompany = new AddressEntity();
                    addressCompany.setStreetName(entity.getAddress().getStreetName());
                    addressCompany.setZipCode(entity.getAddress().getZipCode());
                    addressCompany.setCity(entity.getAddress().getCity());
                    addressCompany.setHouseNumber(entity.getAddress().getHouseNumber());
                    addressCompany.setCountry(entity.getAddress().getCountry());
                    companyEntity.setAddress(addressCompany);
                    return companyEntity;
                })
                .findAny()
                .orElse(null);
    }

    @Override
    public CompanyEntity create(CompanyEntity entity) {
        Long newCompanyID = idGenerator.getAndIncrement();
        entity.setId(newCompanyID);
        DATA.add(entity);
        return entity;
    }
}
