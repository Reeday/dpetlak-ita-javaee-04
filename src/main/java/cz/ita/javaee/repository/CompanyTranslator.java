package cz.ita.javaee.repository;

import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;

public class CompanyTranslator {

    public static CompanyDto entityToDto(CompanyEntity companyEntity) {
        CompanyDto companyDto = new CompanyDto();
        companyDto.setId(companyEntity.getId());
        companyDto.setName(companyEntity.getName());
        companyDto.setCompanyId(companyEntity.getCompanyId());
        companyDto.setVatId(companyEntity.getVatId());
        companyDto.setActive(companyEntity.isActive());
        companyDto.setOwnershipped(companyEntity.isOwnershipped());
        companyDto.setCreated(companyEntity.getCreated());
        companyDto.setNote(companyEntity.getNote());

        AddressDto addressCompany = new AddressDto();
        addressCompany.setStreetName(companyEntity.getAddress().getStreetName());
        addressCompany.setZipCode(companyEntity.getAddress().getZipCode());
        addressCompany.setCity(companyEntity.getAddress().getCity());
        addressCompany.setHouseNumber(companyEntity.getAddress().getHouseNumber());
        addressCompany.setCountry(companyEntity.getAddress().getCountry());
        companyDto.setAddress(addressCompany);

        return companyDto;
    }

    public static CompanyEntity dtoToEntity(CompanyDto companyDto) {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setId(companyDto.getId());
        companyEntity.setName(companyDto.getName());
        companyEntity.setCompanyId(companyDto.getCompanyId());
        companyEntity.setVatId(companyDto.getVatId());
        companyEntity.setActive(companyDto.isActive());
        companyEntity.setOwnershipped(companyDto.isOwnershipped());
        companyEntity.setCreated(companyDto.getCreated());
        companyEntity.setNote(companyDto.getNote());

        AddressEntity addressCompany = new AddressEntity();
        addressCompany.setStreetName(companyDto.getAddress().getStreetName());
        addressCompany.setZipCode(companyDto.getAddress().getZipCode());
        addressCompany.setCity(companyDto.getAddress().getCity());
        addressCompany.setHouseNumber(companyDto.getAddress().getHouseNumber());
        addressCompany.setCountry(companyDto.getAddress().getCountry());
        companyEntity.setAddress(addressCompany);

        return companyEntity;

    }
}
