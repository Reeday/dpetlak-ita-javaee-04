package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Profile("inMemory")
@Repository
public class EmployeeInMemoryRepository implements EmployeeRepository {

    private final List<EmployeeEntity> DATA = new ArrayList<>();

    private AtomicLong idGenerator = new AtomicLong(3);

    @PostConstruct
    public void init() {
        EmployeeEntity employee1 = new EmployeeEntity();
        employee1.setId(1L);
        employee1.setFirstName("Issac");
        employee1.setSurname("Assimov");
        employee1.setActive(true);
        employee1.setNote("Nadace a země");
        employee1.setCreated(LocalDateTime.now());
        AddressEntity addressCompany1 = new AddressEntity();
        addressCompany1.setStreetName("Kubánská");
        addressCompany1.setZipCode("70800");
        addressCompany1.setCity("Ostrava");
        addressCompany1.setHouseNumber("26");
        addressCompany1.setCountry("CZE");
        employee1.setAddress(addressCompany1);

        EmployeeEntity employee2 = new EmployeeEntity();
        employee2.setId(2L);
        employee2.setFirstName("Stephen");
        employee2.setSurname("Hawking");
        employee2.setActive(true);
        employee2.setNote("Stručná historie času");
        employee2.setCreated(LocalDateTime.now());
        AddressEntity addressCompany2 = new AddressEntity();
        addressCompany2.setStreetName("Mongolská");
        addressCompany2.setZipCode("70800");
        addressCompany2.setCity("Poruba");
        addressCompany2.setHouseNumber("11");
        addressCompany2.setCountry("SVK");
        employee2.setAddress(addressCompany2);

        DATA.add(employee1);
        DATA.add(employee2);
    }

    @Override
    public EmployeeEntity findOne(long id) {
        return DATA.stream().filter(companyEntity -> companyEntity.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<EmployeeEntity> find(int offset, int limit) {
        return DATA.subList(offset, Math.min(offset + limit, DATA.size()));
    }

    @Override
    public void delete(long id) {
        DATA.removeIf(employeeDto -> employeeDto.getId().equals(id));
    }

    @Override
    public EmployeeEntity update(EmployeeEntity entity) {
        return DATA.stream()
                .filter(employeeEntity -> employeeEntity.getId().equals(entity.getId()))
                .map(employeeEntity -> {
                    employeeEntity.setFirstName(entity.getFirstName());
                    employeeEntity.setSurname(entity.getSurname());
                    employeeEntity.setActive(entity.isActive());
                    employeeEntity.setNote(entity.getNote());
                    AddressEntity adressEmployee = new AddressEntity();
                    adressEmployee.setStreetName(entity.getAddress().getStreetName());
                    adressEmployee.setHouseNumber(entity.getAddress().getHouseNumber());
                    adressEmployee.setCity(entity.getAddress().getCity());
                    adressEmployee.setZipCode(entity.getAddress().getZipCode());
                    adressEmployee.setCountry(entity.getAddress().getCountry());
                    employeeEntity.setAddress(adressEmployee);
                    return employeeEntity;
                })
                .findAny()
                .orElse(null);
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        Long newEmployeeID = idGenerator.getAndIncrement();
        entity.setId(newEmployeeID);
        DATA.add(entity);
        return entity;
    }
}
