package cz.ita.javaee.repository;

import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.EmployeeEntity;

public class EmployeeTranslator {

    public static EmployeeDto entityToDto(EmployeeEntity employeeEntity) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setFirstName(employeeEntity.getFirstName());
        employeeDto.setSurname(employeeEntity.getSurname());
        employeeDto.setId(employeeEntity.getId());
        employeeDto.setActive(employeeEntity.isActive());
        employeeDto.setCreated(employeeEntity.getCreated());
        employeeDto.setNote(employeeEntity.getNote());

        AddressDto addressDto = new AddressDto();
        addressDto.setStreetName(employeeEntity.getAddress().getStreetName());
        addressDto.setHouseNumber(employeeEntity.getAddress().getHouseNumber());
        addressDto.setCity(employeeEntity.getAddress().getCity());
        addressDto.setZipCode(employeeEntity.getAddress().getZipCode());
        addressDto.setCountry(employeeEntity.getAddress().getCountry());
        employeeDto.setAddress(addressDto);

        return employeeDto;
    }

    public static EmployeeEntity dtoToEntity(EmployeeDto employeeDto) {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setFirstName(employeeDto.getFirstName());
        employeeEntity.setSurname(employeeDto.getSurname());
        employeeEntity.setId(employeeDto.getId());
        employeeEntity.setActive(employeeDto.isActive());
        employeeEntity.setCreated(employeeDto.getCreated());
        employeeEntity.setNote(employeeDto.getNote());

        AddressEntity addressEmployee = new AddressEntity();
        addressEmployee.setStreetName(employeeDto.getAddress().getStreetName());
        addressEmployee.setHouseNumber(employeeDto.getAddress().getHouseNumber());
        addressEmployee.setCity(employeeDto.getAddress().getCity());
        addressEmployee.setZipCode(employeeDto.getAddress().getZipCode());
        addressEmployee.setCountry(employeeDto.getAddress().getCountry());
        employeeEntity.setAddress(addressEmployee);

        return employeeEntity;
    }

}
