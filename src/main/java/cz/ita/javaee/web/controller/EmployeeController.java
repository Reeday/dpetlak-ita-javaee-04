package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.service.api.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<EmployeeDto> findAll() {
        return employeeService.findAll();
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public void create(@RequestBody EmployeeDto employee) {
        employeeService.create(employee);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public void update(@RequestBody EmployeeDto employee) {
        employeeService.update(employee);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        employeeService.delete(id);
    }

}
