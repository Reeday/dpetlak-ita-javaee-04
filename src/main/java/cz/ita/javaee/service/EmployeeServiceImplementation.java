package cz.ita.javaee.service;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.EmployeeTranslator;
import cz.ita.javaee.repository.api.EmployeeRepository;
import cz.ita.javaee.service.api.EmployeeService;
import cz.ita.javaee.service.api.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static cz.ita.javaee.repository.EmployeeTranslator.dtoToEntity;
import static cz.ita.javaee.repository.EmployeeTranslator.entityToDto;

@Service
public class EmployeeServiceImplementation implements EmployeeService {

    private static final String NOTIFICATION_TITLE = "New employee was created in BrainIS";

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional
    public List<EmployeeDto> findAll() {
        List<EmployeeEntity> foundEmployeesEntity = employeeRepository.find(0, Integer.MAX_VALUE);
        return foundEmployeesEntity.stream().map(EmployeeTranslator::entityToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public EmployeeDto create(EmployeeDto employee) {
        employee.setActive(true);
        notificationService.sendNotifiction(NOTIFICATION_TITLE, composeNotification(employee));
        return entityToDto(employeeRepository.create(dtoToEntity(employee)));
    }

    @Override
    @Transactional
    public EmployeeDto update(EmployeeDto employee) {
        EmployeeEntity employeeToUpdate = dtoToEntity(employee);
        return entityToDto(employeeRepository.update(employeeToUpdate));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        employeeRepository.delete(id);
    }

    private static final String composeNotification(EmployeeDto employee) {
        return String.format("New employee name is:  %s %s", employee.getFirstName(), employee.getSurname());
    }
}
