package cz.ita.javaee.service;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.CompanyTranslator;
import cz.ita.javaee.repository.api.CompanyRepository;
import cz.ita.javaee.service.api.CompanyService;
import cz.ita.javaee.service.api.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static cz.ita.javaee.repository.CompanyTranslator.dtoToEntity;
import static cz.ita.javaee.repository.CompanyTranslator.entityToDto;

@Service
public class CompanyServiceImplemenation implements CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImplemenation.class);

    private static final String NOTIFICATION_TITLE = "New company was created in BrainIS";

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional
    public List<CompanyDto> findAll() {
        LOGGER.info("findAll (company) was called");
        List<CompanyEntity> foundCompaniesEntity = companyRepository.find(0, Integer.MAX_VALUE);
        List<CompanyDto> foundCompDtoList = foundCompaniesEntity.stream().map(CompanyTranslator::entityToDto).collect(Collectors.toList());
        LOGGER.debug("findAll is going to return: {}", foundCompDtoList);
        return foundCompDtoList;
    }

    @Override
    @Transactional
    public CompanyDto create(CompanyDto company) {
        LOGGER.info("create (company) was called");
        company.setActive(true);
        LOGGER.info("Notification is going to be sended");
        notificationService.sendNotifiction(NOTIFICATION_TITLE, composeNotification(company));
        LOGGER.info("Notification has been sent");
        CompanyDto createdCompany = entityToDto(companyRepository.create(dtoToEntity(company)));
        LOGGER.debug("create (company) returns: {}", createdCompany);
        return createdCompany;
    }

    @Override
    @Transactional
    public CompanyDto update(CompanyDto company) {
        LOGGER.info("update (company) was called");
        CompanyEntity companyToUpdate = dtoToEntity(company);
        CompanyDto updatedCompany = entityToDto(companyRepository.update(companyToUpdate));
        LOGGER.debug("update (company) is going to return: {}", updatedCompany);
        return updatedCompany;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        LOGGER.info("delete (company) was called");
        companyRepository.delete(id);
        LOGGER.debug("company with ID: {} was deleted", id);
    }

    private static final String composeNotification(CompanyDto company) {
        return String.format("New company name: %s", company.getName());
    }

}
