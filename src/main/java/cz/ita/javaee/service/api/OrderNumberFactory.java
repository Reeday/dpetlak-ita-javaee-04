package cz.ita.javaee.service.api;

public interface OrderNumberFactory {

    String getOrderNumber();
}
