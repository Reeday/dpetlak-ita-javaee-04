package cz.ita.javaee.service.api;

import java.util.List;

import cz.ita.javaee.dto.SubjectDto;

public interface SubjectService {

    List<SubjectDto> findAll();
}
