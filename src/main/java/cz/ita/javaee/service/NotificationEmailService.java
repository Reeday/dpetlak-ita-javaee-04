package cz.ita.javaee.service;

import cz.ita.javaee.service.api.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class NotificationEmailService implements NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImplemenation.class);

    @Autowired
    private MailSender mailSender;

    @Override
    public void sendNotifiction(String title, String text) {
        LOGGER.info("Trying to send notification");
        try {
            mailSender.send(newNotification(title, text));
        } catch (MailSendException e) {
            LOGGER.warn("Error occured during notification send (MailSendException)");
        }
    }

    /*Creates new msg*/
    private SimpleMailMessage newNotification(String title, String text) {
        SimpleMailMessage newMsg = new SimpleMailMessage();
        newMsg.setTo("david.petlak@gmail.com");
        newMsg.setSubject(title);
        newMsg.setText(text);
        return newMsg;
    }
}
