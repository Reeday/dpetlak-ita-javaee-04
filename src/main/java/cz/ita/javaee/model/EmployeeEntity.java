package cz.ita.javaee.model;

import javax.persistence.*;

@Entity
@Table(name = "\"EMPLOYEE\"")
public class EmployeeEntity extends SubjectEntity {

    @Column(name = "\"FIRST_NAME\"", nullable = false, length = 20)
    private String firstName;
    @Column(name = "\"SURNAME\"", nullable = false, length = 20)
    private String surname;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "streetName", column = @Column(name = "\"STREET_NAME\"", length = 40)),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "\"HOUSE_NUMBER\"", nullable = false, length = 10)),
            @AttributeOverride(name = "city", column = @Column(name = "\"CITY\"", nullable = false, length = 60)),
            @AttributeOverride(name = "zipCode", column = @Column(name = "\"ZIP_CODE\"", nullable = false, length = 10)),
            @AttributeOverride(name = "country", column = @Column(name = "\"COUNTRY\"", nullable = false, length = 3)),
    })
    private AddressEntity address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }
}
