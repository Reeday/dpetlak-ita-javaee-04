package cz.ita.javaee.model;


import javax.persistence.*;

@Entity
@Table(name = "\"COMPANY\"")
public class CompanyEntity extends SubjectEntity {
    @Column(name = "\"ACTIVE\"")
    private boolean active = true;
    @Column(name = "\"NOTE\"")
    private String note;
    @Column(name = "\"NAME\"", nullable = false)
    private String name;
    @Column(name = "\"COMPANY_ID\"", nullable = false)
    private String companyId;
    @Column(name = "\"VAT_ID\"")
    private String vatId;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "streetName", column = @Column(name = "\"STREET_NAME\"", length = 40)),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "\"HOUSE_NUMBER\"", nullable = false, length = 10)),
            @AttributeOverride(name = "city", column = @Column(name = "\"CITY\"", nullable = false, length = 60)),
            @AttributeOverride(name = "zipCode", column = @Column(name = "\"ZIP_CODE\"", nullable = false, length = 10)),
            @AttributeOverride(name = "country", column = @Column(name = "\"COUNTRY\"", nullable = false, length = 3)),
    })

    private AddressEntity address;
    @Column(name = "\"OWNERSHIPPED\"")
    private boolean ownershipped;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public boolean isOwnershipped() {
        return ownershipped;
    }

    public void setOwnershipped(boolean ownershipped) {
        this.ownershipped = ownershipped;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
