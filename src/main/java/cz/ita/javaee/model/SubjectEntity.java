package cz.ita.javaee.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SubjectEntity extends AbstractEntity {

    @Column(name = "\"ACTIVE\"", nullable = false)
    private boolean active;
    @Column(name = "\"NOTE\"", length = 300)
    private String note;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
